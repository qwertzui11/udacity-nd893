# Project Navigation

## Source

[gitlab](https://gitlab.com/qwertzui11/udacity-nd893)

## Goal

The goal of this Playground is to get as much Bananas as possible in a timeframe.
The environment provides us the values of a couple of rays to determine if there's a banana or wall and our speed.
These rays, speed and if it's a wall or banana, are the *states* coming from the environment.
We can use 4 actions to move the agent in the environment. The available 4 actions are: `forward`, `backward`, `turn left` and `turn right`.
We can collect the bananas by running into them. There are two kind of bananas. yellow ones, which give a reward of +1 and blue ones which give a reward of -1.
The project is solved when we receive an average reward (over 100 episodes) of at least +13.

## Getting started

The only supported and tested platform is [conda](https://www.anaconda.com/) on Linux.

### Get the Unity Environments.

First we need to get the environment. In which we will [collect Bananas](https://www.youtube.com/watch?v=heVMs3t9qSk) in.
The [headless](https://s3-us-west-1.amazonaws.com/udacity-drlnd/P1/Banana/Banana_Linux_NoVis.zip)
and [with rendering](https://s3-us-west-1.amazonaws.com/udacity-drlnd/P1/Banana/Banana_Linux.zip)

```bash
curl -LO 'https://s3-us-west-1.amazonaws.com/udacity-drlnd/P1/Banana/Banana_Linux.zip'
curl -LO 'https://s3-us-west-1.amazonaws.com/udacity-drlnd/P1/Banana/Banana_Linux_NoVis.zip'
unzip Banana_Linux.zip
unzip Banana_Linux_NoVis.zip
```

### Set up conda env

Next we install all needed to train our agent and replay the trained data.
You must use python `3.6`. Newer version [get the runtime error `UnicodeDecodeError`](https://knowledge.udacity.com/questions/524468)

```bash
conda create --name python-3_6 python=3.6
conda activate python-3_6
pip install unityagents torch numpy matplotlib
```

## Instructions

### training

for training run

```bash
python train.py
```

This program will train using the headless Banana environment. After the program reached an average score of `13.0` for the last 100 episodes it will end.
Before ending it will save its Network to `checkpoint.pth` and show a graph showing the score of each episode.

### play

Test the resulting network using `play.py`. This program loads the `checkpoint.pth` and runs it using the Visual environment.

```bash
python play.py
```


