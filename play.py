from environment import load_environment
from dqn_agent import Agent
import torch

env = load_environment(visual=True)
brain_name = env.brain_names[0]
brain = env.brains[brain_name]
env_info = env.reset(train_mode=False)[brain_name]
state = env_info.vector_observations[0]
state_size = len(state)
action_size = brain.vector_action_space_size

agent = Agent(state_size=state_size, action_size=action_size, seed=0)
loaded_checkpoint = torch.load('checkpoint.pth')
agent.qnetwork_local.load_state_dict(loaded_checkpoint)

for _ in range(1):
    env_info = env.reset(train_mode=False)[brain_name]
    state = env_info.vector_observations[0]
    score = 0
    while True:
        action = agent.act(state)
        env_info = env.step(action)[brain_name]        # send the action to the environment
        next_state = env_info.vector_observations[0]   # get the next state
        reward = env_info.rewards[0]                   # get the reward
        done = env_info.local_done[0]                  # see if episode has finished
        state = next_state
        score += reward
        if done:
            break
    print(f'finished with score: {score}')

env.close()


