from unityagents import UnityEnvironment

def load_environment(visual):
    if visual:
        file_name = "Banana_Linux/Banana.x86_64"
    else:
        file_name = "Banana_Linux_NoVis/Banana.x86_64"
    return UnityEnvironment(file_name=file_name)
