# Report

## Learning Algorithm

### network

There are two fully connected networks, declared in [`model.py`](model.py).
It consists of an input layer (37 nodes) representing the environment, two fully connected hidden layers (2x64 nodes) and an output layer respresenting the actions (4 Nodes (forward, left, right, back)).
Between the hidden layers and the output layer the ReLU activation function is used.

The network describes the q-state-action-value-function using weights and an activation function, trying to estimate the ideal policy.
The activation function is needed to approximate non-linear policies.

### replay buffer

When training, we are using a replay buffer. In that buffer we are collecting up to `10.000` tuples, called `experience`, of the following information:
- state. Which is an array of the size 37.
- action. Which describes the chosen action: `[0..3]`
- reward. `1` for a yellow banana. and `-1` for a blue banana.
- next state
- done, which is `0` for an ongoing episode, and `1` if it's the last step in an episode.

If the buffer exceeds `10.000` entries, the oldest gets removed.

Using the method `sample` gets you a batch of 64 randomly chosen `experience`s.

### training

The class `Agent` contains two networks. One is called `qnetwork_local` and one is called `qnetwork_target`.
The network `qnetwork_target` is the network with the fixed weights.
We start training the network by gathering at least 64 (random) experiences. (Experience replay)

Next we want to use these experiences to calculate the error of the network. The error-target is the fixed weight network `qnetwork_target`. For each experience we calculate the `max q(S,a,w-)` and save it into `Q_targets_next`. check `Agent.learn` in [`dqn_agent.py`](dqn_agent.py). `Q_targets_next` contains the 64 (batch size) max-q-values of the next state. (Sarsa Max)

The TD-target uses the *next* state to calculate itself. We implemented it in two ways.
- Once against the fixed target network:
`Q_targets` contains the TD-target (temporal difference) of the next state. (`R + gamma * max q(S,a,w-)`). (Bellman equation)
`1-dones` means we ignore the last episode.
- And once using double DQ for less oscillation.
`Q_targets` contains the TD-Target using the action evaluated against the local-network. Then we are using the action to get the Q value from the target-network.

`Q_expected` contains the `q(S,A,w)`. describing the Q values of the network we want to optimize. We choose the q values, depending on the *current* state/action that we experienced.

Then we calculate the mean-squared-error (over all 64 entries) between our target (`Q_targets` and the network we want to approximate `Q_expected`) and backpropigate it to the network (`qnetwork_local`). (apply derived error. apply inverted gradient)

In the last step we (soft `tau=0.001`) change the target (fixed w) network to the changed (local) network.

Using `Training against a fixed target` we have been able to solve the environment in around 538 episodes.
Adding `Double DQ` we have been able to solve the environment in around 528 episodes.

## Plot of Rewards

### Without Double DQN scores

![without double dqn scores](no_opt.svg "Without Double DQN scores")

### Double DQN scores

![with double dqn scores](with_double_dqn.svg "Double DQN scores")

## Ideas for Future Work

- learn from pixels
- Dueling DQN
- PER (Prioritized Experience Replay)

